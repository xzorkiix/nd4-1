class Pokemon {
    constructor(name, level){
        this.name = name;
        this.level = level;
    };
    valueOf(){
        return this.level;
    };
    show(){
        console.log(`pokemon ${this.name} with ${this.level} level`);
    }
}

class PokemonList extends Array {

    constructor(...items){
       super(); // if you try to access this before super() is called, 
                // iojs will throw an error: ReferenceError: this is not defined. 
                // https://github.com/nodejs/node/issues/1723
       this.push(...items);     
    }

    add(name, level){
        this.push(new Pokemon(name, level));
    }
    show(){
        this.forEach(e => e.show(), this);
        console.log(`Total size: ${this.length} pokemons`);
    }
    max(){
        let maxLvl = Math.max(...this);
        return this.find(e => e.valueOf() === maxLvl) || new Pokemon();
    }
    push(...items){
        items.forEach( e => {
            if(e instanceof Pokemon)
                super.push(e);
            if(Array.isArray(e))
                this.push(...e);
        });        
    }
    transfer(item, toList){     
        if(item instanceof Pokemon)
            return this.transfer(this.indexOf(item), toList);
        if(this[item])
            toList.push(this.splice(item,1));
    }
    
}

function show(pokemonlist, name) {
    console.log('show of list %s', name);
    pokemonlist.show();
    console.log('max is:');
    pokemonlist.max().show();
    console.log();
}

var find = new PokemonList();
var lost = new PokemonList(new Pokemon('Пикачу', 10), new Pokemon('Слоупок', 15), 15);
lost.add('Сквидл', 3);
lost.push(new Pokemon('Пикачу', 33), new Pokemon('Слоупок', 55));

console.log('Состав список lost и find после инициализации');
show(lost, 'lost');
show(find, 'find');

lost.transfer(lost.max(), find);

console.log('После перемещения лучшего покемона из lost-списка в find');
show(lost, 'lost');
show(find, 'find');

/* Пример вывода скрипта

Состав список lost и find после инициализации                                                                                     
show of list lost                                                                                                                 
pokemon Пикачу with 10 level                                                                                                      
pokemon Слоупок with 15 level                                                                                                     
pokemon Сквидл with 3 level                                                                                                       
pokemon Пикачу with 33 level                                                                                                      
pokemon Слоупок with 55 level                                                                                                     
Total size: 5 pokemons                                                                                                            
max is:                                                                                                                           
pokemon Слоупок with 55 level                                                                                                     
                                                                                                                                  
show of list find                                                                                                                 
Total size: 0 pokemons                                                                                                            
max is:                                                                                                                           
pokemon undefined with undefined level

После перемещения лучшего покемона из lost-списка в find 
show of list lost                                                                                                                 
pokemon Пикачу with 10 level                                                                                                      
pokemon Слоупок with 15 level                                                                                                     
pokemon Сквидл with 3 level                                                                                                       
pokemon Пикачу with 33 level                                                                                                      
Total size: 4 pokemons                                                                                                            
max is:                                                                                                                           
pokemon Пикачу with 33 level                                                                                                      
                                                                                                                                  
show of list find                                                                                                                 
pokemon Слоупок with 55 level                                                                                                     
Total size: 1 pokemons                                                                                                            
max is:                                                                                                                           
pokemon Слоупок with 55 level  

*/